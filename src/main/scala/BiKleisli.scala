import cats.Monad
import cats.data.Kleisli
import cats.implicits._

case class BiKleisli[F[_], G[_], A, B](forward: Kleisli[F, A, B], backward: Kleisli[G, B, A]) { self =>
  def >>>[C](that: BiKleisli[F, G, B, C])(implicit F: Monad[F], G: Monad[G]): BiKleisli[F, G, A, C] = BiKleisli(
    self.forward >>> that.forward,
    that.backward >>> self.backward
  )

  def first[C](implicit F: Monad[F], G: Monad[G]): BiKleisli[F, G, (A, C), (B, C)] = BiKleisli(
    self.forward.first[C],
    self.backward.first[C]
  )

  def second[C](implicit F: Monad[F], G: Monad[G]): BiKleisli[F, G, (C, A), (C, B)] = BiKleisli(
    self.forward.second[C],
    self.backward.second[C]
  )

}
