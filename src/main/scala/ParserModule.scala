import BiArrow._
import cats.Id
import cats.data.{Kleisli, NonEmptyList}
import cats.implicits._

object ParserModule {
  type Parser[A, B] = BiKleisli[Either[String, *], Id, A, (A, B)]
  type ParserList[A, B] = BiArrow[A, (A, List[B])]

  implicit class Syntax[A, B](self: Parser[A, B]) {
    def <~>[C](right: Parser[A, C]): Parser[A, (B, C)] = parseProduct(self, right)
    def <~[C](defaultRight: C, right: Parser[A, C]): Parser[A, B] = parseLeft(defaultRight, self, right)
    def ~>[C](defaultLeft: B, right: Parser[A, C]): Parser[A, C] = parseRight(defaultLeft, self, right)
    def |[C](right: Parser[A, C]): Parser[A, Either[B, C]] = choice(self, right)
  }

  def parseProduct[A, B, C](left: Parser[A, B], right: Parser[A, C]): Parser[A, (B, C)] =
    left >>>
    right.first >>>
    assoc.toBiKleisli >>>
    swap.second.toBiKleisli

  def parseLeft[A, B, C](defaultRight: C, left: Parser[A, B], right: Parser[A, C]): Parser[A, B] =
    left >>>
    right.first >>>
    dropSecond(defaultRight).first.toBiKleisli

  def parseRight[A, B, C](defaultLeft: B, left: Parser[A, B], right: Parser[A, C]): Parser[A, C] =
    left >>>
    right.first >>>
    dropSecond(defaultLeft).toBiKleisli

  def foldLeft[A, B](parser: Parser[A, B]): BiArrow[A, (A, List[B])] = BiArrow.foldLeft[A, B](
    (acc, b) => parser.backward.run(acc, b),
    parser.forward.run(_) match { case Right(value) => value }
  ).inv

  def parserListToNonEmptyParser[A, B](error: String, parserList: ParserList[A, B]): Parser[A, NonEmptyList[B]] = BiKleisli(
    Kleisli {
      parserList.forward(_) match {
        case (_, Nil) => Left(error)
        case (a, b :: bs) => Right(a, NonEmptyList(b, bs))
      }
    },
    Kleisli {
      case (a, bs) => parserList.backward(a, bs.toList)
    }
  )

  def zeroOrMore[A, B](parser: Parser[A, B]): Parser[A, List[B]] =
    (foldLeft(parser) >>> reverseArrow.second).toBiKleisli

  def oneOrMore[A, B](error: String)(parser: Parser[A, B]): Parser[A, NonEmptyList[B]] =
    parserListToNonEmptyParser(error, foldLeft(parser) >>> reverseArrow.second)

  def filterArrow[A](f: A => Boolean, error: String): BiKleisli[Either[String, *], Id, A, A] = BiKleisli(
    Kleisli {
      case a if f(a) => Right(a)
      case _ => Left(error)
    },
    Kleisli[Id, A, A](identity)
  )

  def satisfy[A, B](f: B => Boolean, error: String)(parser: Parser[A, B]): Parser[A, B] =
    parser >>> filterArrow(f, error).second

  // use with foldLeft
  def sep[A, B, C](sep: Parser[A, C])(parser: Parser[A, B]): Parser[A, (B, List[(C, B)])] =
    parser <~> zeroOrMore(sep <~> parser)

  def sepBy[A, B, C](defaultSep: C, sep: Parser[A, C])(parser: Parser[A, B]): Parser[A, NonEmptyList[B]] =
    parser <~> zeroOrMore(sep ~> (defaultSep, parser)) >>> BiArrow.NelToPair.inv.second.toBiKleisli

  def choiceArrow[A, B, C]: ((Either[String, (A, B)], Either[String, (A, C)])) => Either[String, (A, Either[B, C])] = {
    case (Right((rest, b)), _) => Right(rest, Left(b))
    case (_, Right((rest, c))) => Right(rest, Right(c))
    case (Left(err1), Left(err2)) => Left(s"$err1 or $err2")
  }

  def choice[A, B, C](left: Parser[A, B], right: Parser[A, C]): Parser[A, Either[B, C]] = BiKleisli(
    Kleisli((left.forward.run &&& right.forward.run) >>> choiceArrow),
    Kleisli {
      case (a, Left(b)) => left.backward(a, b)
      case (a, Right(c)) => right.backward(a, c)
    }
  )

  def between[A, B, C, D](defaultOpen: C, open: Parser[A, C], defaultClose: D, close: Parser[A, D])(parser: Parser[A, B]): Parser[A, B] =
    open ~> (defaultOpen, parser) <~ (defaultClose, close)

  def anyChar: Parser[String, Char] = BiKleisli(
    Kleisli {
      str =>
        str.headOption match {
          case Some(ch) => Right(str.drop(1), ch)
          case None => Left("String is empty")
        }
    },
    Kleisli {
      case (str, ch) => ch + str
    }
  )
}
