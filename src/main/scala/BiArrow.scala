import cats.data.{Kleisli, NonEmptyList}
import cats.implicits._
import cats._

import scala.annotation.tailrec

case class BiArrow[A, B](forward: A => B, backward: B => A) { self =>
  def >>>[C](that: BiArrow[B, C]): BiArrow[A, C] = BiArrow(
    self.forward >>> that.forward,
    that.backward >>> self.backward
  )

  def first[C]: BiArrow[(A, C), (B, C)] = BiArrow(
  self.forward.first[C],
  self.backward.first[C]
  )

  def second[C]: BiArrow[(C, A), (C, B)] = BiArrow(
    self.forward.second[C],
    self.backward.second[C]
  )

  def inv: BiArrow[B, A] = BiArrow(
    self.backward,
    self.forward
  )

  def toBiKleisli[F[_], G[_]](implicit F: Monad[F], G: Monad[G]): BiKleisli[F, G, A, B] = BiKleisli(
    Kleisli(self.forward.map(_.pure[F])),
    Kleisli(self.backward.map(_.pure[G]))
  )

}

object BiArrow {
  def assoc[A, B, C]: BiArrow[((A, B), C), (A, (B, C))] = BiArrow(
    { case ((a, b), c) => (a, (b, c)) },
    { case (a, (b, c)) => ((a, b), c) },
  )

  def swap[A, B]: BiArrow[(A, B), (B, A)] = BiArrow(
    { case (a, b) => (b, a) },
    { case (b, a) => (a, b) }
  )

  def dropFirst[A, B](default: A): BiArrow[(A, B), B] = BiArrow(
    { case (_, b) => b },
    { b => (default, b) }
  )

  def dropSecond[A, B](default: B): BiArrow[(A, B), A] = BiArrow(
    { case (a, _) => a },
    { a => (a, default) }
  )

  def NelToPair[A]: BiArrow[NonEmptyList[A], (A, List[A])] = BiArrow(
    nel => (nel.head, nel.tail),
    { case (head, tail) => NonEmptyList(head, tail) }
  )

  def reverseArrow[A]: BiArrow[List[A], List[A]] = BiArrow(_.reverse, _.reverse)

  def foldLeft[A, B](fold: (A, B) => A, unfold: PartialFunction[A, (A, B)]): BiArrow[(A, List[B]), A] = BiArrow(
    { case (a, bs) => bs.foldLeft(a)(fold) },
    {
      @tailrec
      def go(a: A, acc: List[B]): (A, List[B]) = {
        unfold.lift(a) match {
          case None => (a, acc)
          case Some((a1, b)) => go(a1, b :: acc)
        }

      }
      go(_, Nil)
    }
  )

}