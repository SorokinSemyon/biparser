import ParserModule._
import cats.data.NonEmptyList

object ArithmeticTest extends App {
  sealed trait Expr
  case class Number(value: Int) extends Expr
  case class Add(l: Expr, r: Expr) extends Expr
  case class Sub(l: Expr, r: Expr) extends Expr
  case class Mult(l: Expr, r: Expr) extends Expr
  case class Div(l: Expr, r: Expr) extends Expr
  case class SubExpr(expr: Expr) extends Expr

  def digitChar: Parser[String, Char] = satisfy[String, Char](_.isDigit, "excepted digit")(anyChar)

  def digits: Parser[String, NonEmptyList[Char]] = oneOrMore("expected digits")(digitChar)

  def digitsToNumberArrow: BiArrow[NonEmptyList[Char], Int] = BiArrow(
    digits => digits.toList.mkString.toInt,
    n => NonEmptyList.fromListUnsafe(n.toString.toCharArray.toList)
  )

  def number: Parser[String, Int] = digits >>> digitsToNumberArrow.second.toBiKleisli

  def char(char: Char): Parser[String, Char] = satisfy[String, Char](_ == char, s"excepted '$char'")(anyChar)

  def space: Parser[String, Char] = char(' ')

  def spaces  = zeroOrMore(space)

  def numberWithSpaces: Parser[String, Int] = spaces ~> (List.empty, number) <~ (List.empty, spaces)

  def intToNumberArrow: BiArrow[Int, Expr] = BiArrow(
    n => Number(n),
    { case Number(n) => n }
  )

  def numberExpr: Parser[String, Expr] = numberWithSpaces >>> intToNumberArrow.second.toBiKleisli

  def multAndDivSep: Parser[String, Expr] => Parser[String, (Expr, List[(Either[Char, Char], Expr)])] =
    sep(char('*') | char('/'))

  def multAndDivArrow: BiArrow[(Expr, List[(Either[Char, Char], Expr)]), Expr] =
    BiArrow.foldLeft(
    {
      case (l, (Left('*'), r)) => Mult(l, r)
      case (l, (Right('/'), r)) => Div(l, r)
    },
    {
      case Mult(l, r) => (l, (Left('*'), r))
      case Div(l, r) => (l, (Right('/'), r))
    }
  )

  def multAndDiv: Parser[String, Expr] => Parser[String, Expr] = multAndDivSep(_) >>> multAndDivArrow.second.toBiKleisli

  def addAndSubSep: Parser[String, Expr] => Parser[String, (Expr, List[(Either[Char, Char], Expr)])] =
    sep(char('+') | char('-'))

  def addAndSubArrow: BiArrow[(Expr, List[(Either[Char, Char], Expr)]), Expr] =
    BiArrow.foldLeft(
    {
      case (l, (Left('+'), r)) => Add(l, r)
      case (l, (Right('-'), r)) => Sub(l, r)
    },
    {
      case Add(l, r) => (l, (Left('+'), r))
      case Sub(l, r) => (l, (Right('-'), r))
    }
  )

  def addAndSub: Parser[String, Expr] => Parser[String, Expr] = addAndSubSep(_) >>> addAndSubArrow.second.toBiKleisli

  def bracket: Parser[String, Expr] => Parser[String, Expr] =
    between('(', char('('), ')', char(')'))

  def exprToSubExprArrow: BiArrow[Expr, Expr] = BiArrow(
    expr => SubExpr(expr),
    { case SubExpr(expr) => expr}
  )

  def simpleExpr: Parser[String, Expr] = addAndSub(multAndDiv(numberExpr))

  def subExpr: Parser[String, Expr] =
    spaces ~> (List.empty, bracket(simpleExpr)) <~ (List.empty, spaces) >>> exprToSubExprArrow.second.toBiKleisli

  def simpleAndSubArrow: BiArrow[Either[Expr, Expr], Expr] = BiArrow(
    {
      case Left(expr) => expr
      case Right(expr) => expr
    },
    {
      case subExpr@SubExpr(_) => Right(subExpr)
      case expr => Left(expr)
    }
  )

  def simpleAndSubExpr: Parser[String, Expr] = (simpleExpr | subExpr) >>> simpleAndSubArrow.second.toBiKleisli

  def expr: Parser[String, Expr] = addAndSub(multAndDiv(simpleAndSubExpr))
  println(expr.forward("1 - ( 2 + 3 / 4 ) * 5"))
  println(simpleExpr.forward("1 - 2 + 3 / 4 * 5"))
  println(expr.backward("", Sub(Number(1),Mult(SubExpr(Add(Number(2),Div(Number(3),Number(4)))),Number(5)))))
}
